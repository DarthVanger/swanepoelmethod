Swanepoel method
=================
This application is an attempt to program Swanepoel's method for finding optical constants of thin films.

Used frameworks
---------------
The app is based on Symfony2 php framework and AngularJS javascript framework.
All the computations are done on javascript. Php is used only for data files managment.